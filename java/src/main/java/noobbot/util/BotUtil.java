package noobbot.util;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import noobbot.domain.CarPosition;
import noobbot.domain.Piece;
import noobbot.domain.Race;

public class BotUtil {

	

	public static CarPosition getBotPosition(List<CarPosition> carPositions, String botName){
		if(carPositions == null || botName == null) return null;
		
		for(CarPosition cp : carPositions){
			if(cp.getId().getName().equals(botName)) return cp;
		}
		System.out.println("bot not found name=" + botName);
		return null;
	}
	
	public static Piece getNextPiece(Race race, CarPosition currentPosition){
		LinkedList<Piece> allPieces = race.getTrack().getPieces();
		int nextPosition = currentPosition.getPiecePosition().getPieceIndex()+2;
		if(nextPosition >= allPieces.size() ){
			nextPosition = 0;
		}
		return race.getTrack().getPieces().get(nextPosition);
	}
	
	public static void printUniqueTrackAngle(Race race){
		Set<Double> angles = new HashSet<Double>();
		for( Piece p : race.getTrack().getPieces()){
			if(!angles.contains(p.getAngle())){
				angles.add(p.getAngle());
				System.out.println("angle: " + p.getAngle());
			}
		}
	}
	
	public static boolean hasTurnsAhead(int currentPiece, LinkedList<Piece> trackPieces){
//		
//		for(int i = currentPiece; i< trackPieces.size(); i++){
//			if(trackPieces.get(i).getAngle() != 0) return true;
//		}
		for(int i=currentPiece; i< (currentPiece+6); i++){
			Piece nextPiece = trackPieces.get((currentPiece + i) % trackPieces.size());	
			if(nextPiece.getAngle() != 0) return true;
		}
		
		return false;
	}
}
