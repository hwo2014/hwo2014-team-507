package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import noobbot.domain.CarPosition;
import noobbot.domain.CarPositions;
import noobbot.domain.Id;
import noobbot.domain.Msg;
import noobbot.domain.Piece;
import noobbot.domain.Race;
import noobbot.util.BotUtil;

import com.google.gson.Gson;
import com.ts.bot.TraqStarsBot;

public class Main {
    public static void main(String... args) throws IOException {
//        String host = args[0];
//        int port = Integer.parseInt(args[1]);
//        String botName = args[2];
//        String botKey = args[3];
        
//        String host = "testserver.helloworldopen.com";
//        int port = Integer.parseInt("8091");
//        String botName = "trackstarsbot";
//        String botKey = "6sJ4pZvYAUIApw";
//
//        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
//
//        final Socket socket = new Socket(host, port);
//        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
//
//        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
//
//        new Main(reader, writer, new Join(botName, botKey));
    	
    	TraqStarsBot.execute(args);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
        Race race = null;
        int position = 0;
        int lap = 0;
        String ourBot = join.name;
        while((line = reader.readLine()) != null) {
        	System.out.println("line1: " + line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	
            	//This part is where the bulk of the logic will be, it should probably be moved somewhere else.
            	CarPositions carPositions = gson.fromJson(line, CarPositions.class);
                CarPosition ourBotPosition = BotUtil.getBotPosition(carPositions.getData(), ourBot);
                Piece nextPiece = BotUtil.getNextPiece(race, ourBotPosition);
                double angle = nextPiece.getAngle(); //ourBotPosition.getAngle();
                if(angle > 23 || angle < -23){
                	send(new Throttle(0.5));
                }
                else if(angle < 23 && angle > .25 || 
                		angle > -23 && angle < -.25 ){
                	send (new Throttle(.80));
                }
                else{
                	send(new Throttle(.93));
                }
                
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                Msg msg = gson.fromJson(line, Msg.class);
                race = msg.getData().getRace();
                //BotUtil.printUniqueTrackAngle(race);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
            	System.out.println("MsgType: " + msgFromServer.msgType + " Sending ping");
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
    	String output = msg.toJson();
    	System.out.println("sending: " + output);
        writer.println(output);
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}



class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Switch extends SendMsg {
    private String value;  //left or right

    public Switch(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switch";
    }
}