package noobbot.domain;

public class TurboAvailable {

	private String msgType;
	private TurboData data;
	
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public TurboData getData() {
		return data;
	}
	public void setData(TurboData data) {
		this.data = data;
	}
	
	
}
