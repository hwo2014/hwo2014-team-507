package noobbot.domain;

public class TurboData {

	private double turboDurationMilliseconds;
	private double turboDurationTicks;
	private double turboFactor;
	
	public double getTurboDurationMilliseconds() {
		return turboDurationMilliseconds;
	}
	public void setTurboDurationMilliseconds(double turboDurationMilliseconds) {
		this.turboDurationMilliseconds = turboDurationMilliseconds;
	}
	public double getTurboDurationTicks() {
		return turboDurationTicks;
	}
	public void setTurboDurationTicks(double turboDurationTicks) {
		this.turboDurationTicks = turboDurationTicks;
	}
	public double getTurboFactor() {
		return turboFactor;
	}
	public void setTurboFactor(double turboFactor) {
		this.turboFactor = turboFactor;
	}
}
