package noobbot.domain;

public class Lane {
	private int distanceFromCenter;
	private int index;
	
	private int startLaneIndex;
	private int endLaneIndex;
	
//	public Lane(int distanceFromCenter, int index){
//		this.distanceFromCenter = distanceFromCenter;
//		this.index = index;
//	}

	public int getDistanceFromCenter() {
		return distanceFromCenter;
	}

	public void setDistanceFromCenter(int distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getStartLaneIndex() {
		return startLaneIndex;
	}

	public void setStartLaneIndex(int startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}

	public int getEndLaneIndex() {
		return endLaneIndex;
	}

	public void setEndLaneIndex(int endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}
}
