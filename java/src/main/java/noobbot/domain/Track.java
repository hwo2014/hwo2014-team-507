package noobbot.domain;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Track {

	private String id;
	private String name;
	private LinkedList<Piece> pieces = new LinkedList<Piece>();
	private List<Lane> lanes = new ArrayList<Lane>();
	private StartingPoint startingPoint;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<Lane> getLanes() {
		return lanes;
	}
	public void setLanes(List<Lane> lanes) {
		this.lanes = lanes;
	}
	public StartingPoint getStartingPoint() {
		return startingPoint;
	}
	public void setStartingPoint(StartingPoint startingPoint) {
		this.startingPoint = startingPoint;
	}
	public LinkedList<Piece> getPieces() {
		return pieces;
	}
	public void setPieces(LinkedList<Piece> pieces) {
		this.pieces = pieces;
	}
}
