package noobbot.domain;

import java.util.List;

public class CarPositions {
	private String msgType;
	private List<CarPosition> data;

	public List<CarPosition> getData() {
		return data;
	}

	public void setData(List<CarPosition> data) {
		this.data = data;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public CarPosition getBotPosition(String botName) {
		if(data == null || botName == null) return null;
		
		for(CarPosition cp : data){
			if(cp.getId().getName().equals(botName)) return cp;
		}
		
		System.out.println("bot not found name=" + botName);
		return null;
	}
	
	
}
