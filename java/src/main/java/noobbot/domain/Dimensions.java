package noobbot.domain;

public class Dimensions {
	private int length;
	private int width;
	private int guideFlagPosition;
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getGuideFlagPosition() {
		return guideFlagPosition;
	}
	public void setGuideFlagPosition(int guideFlagPosition) {
		this.guideFlagPosition = guideFlagPosition;
	}
	
	
}
