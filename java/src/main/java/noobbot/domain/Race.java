package noobbot.domain;

import java.util.ArrayList;
import java.util.List;

public class Race {
	
	private Track track;
	private List<Car> cars = new ArrayList<Car>();
	private RaceSession raceSession;
	
	
	public List<Car> getCars() {
		return cars;
	}
	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	public RaceSession getRaceSession() {
		return raceSession;
	}
	public void setRaceSession(RaceSession raceSession) {
		this.raceSession = raceSession;
	}
	public Track getTrack() {
		return track;
	}
	public void setTrack(Track track) {
		this.track = track;
	}
}
