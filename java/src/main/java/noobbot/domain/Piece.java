package noobbot.domain;

public class Piece {
	
	private int length;
	private boolean isSwitch = false;
	private double angle = 0;
	private int radius = 0;
	
//	public Piece(int length){
//		this.length = length;
//	}
//	
//	public Piece(int length, double angle, int radius, boolean isSwitch){
//		this.length = length;
//		this.angle = angle;
//		this.radius = radius;
//		this.isSwitch = isSwitch;
//	}
//	
//	public Piece(double angle, int radius, boolean isSwitch){
//		this.angle = angle;
//		this.radius = radius;
//		this.isSwitch = isSwitch;
//	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSwitch() {
		return isSwitch;
	}

	public void setSwitch(boolean isSwitch) {
		this.isSwitch = isSwitch;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public boolean isTurn() {
		return angle != 0 && length == 0;
	}
	
	public double getArcLength() {
		double radians = angle * 0.0174532925;
		return radians * radius;
	}
	
}
