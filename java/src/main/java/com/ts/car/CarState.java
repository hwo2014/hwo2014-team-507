package com.ts.car;

import java.util.ArrayList;
import java.util.List;

import noobbot.domain.CarPosition;
import noobbot.domain.Piece;
import noobbot.domain.Track;
import noobbot.domain.TurboAvailable;

public class CarState {
	private Track track;

	private boolean crashed;
	private boolean enteredNewPiece;
	
	private int pieceIndex;
	private int raceTicks;
	private int lapTicks;
	private int crashes;
	private int laneDistanceOffset;
	private int timeBetweenTicks = 1; // hardcoded for now as time is relative only to server ticks
	
	private double previousAngle;
	private double angle;
	private double speed;
	private double previousSpeed;
	private double acceleration;
	private double distance;
	
	private List<Double> raceSpeeds;
	private List<Double> lapSpeeds;
	
	// TODO: track all speeds to see avg speed for track? or on a per-lap basis?

	private CarPosition position;
	private CarPosition previousPosition;
	
	private Piece piece;
	private Piece previousPiece;
	private TurboAvailable turbo;
	
	public CarState(Track track) {
		this.track = track;
		raceSpeeds = new ArrayList<Double>();
		lapSpeeds = new ArrayList<Double>();
	}
	
	public boolean isCrashed() {
		return crashed;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public double getAcceleration() {
		return acceleration;
	}
	
	public int getCrashes() {
		return crashes;
	}
	
	public int getTicks() {
		return raceTicks;
	}
	
	public double getAngle() {
		return angle;
	}
	
	public double getPreviousSpeed(){
		return previousSpeed;
	}
	
	/**
	 * positive means car is swinging away from the track
	 * negative means car is returning to normal position
	 */
	public double getAngleChange() {
		return Math.abs(angle) - Math.abs(previousAngle);
	}
	
	public int getPieceIndex() {
		return pieceIndex;
	}
	
	public Piece getPiece() {
		return piece;
	}
	
	public CarPosition getPosition() {
		return position;
	}
	
	public boolean getEnteredNewPiece() {
		return enteredNewPiece;
	}
	
	public double getAverageLapSpeed() {
		double sum = 0;
		
		for(Double speed : lapSpeeds) {
			sum += speed.doubleValue();
		}
		
		return sum / lapTicks;
	}
	
	public double getAverageRaceSpeed() {
		double sum = 0;
		
		for(Double speed : raceSpeeds) {
			sum += speed.doubleValue();
		}
		
		return sum / raceTicks;
	}
	
	public void updateState(CarPosition botPosition) {
		raceTicks++;
		lapTicks++;
		
		// update previous values
		previousPosition = position;
		previousPiece = piece;
		previousSpeed = speed;
		previousAngle = angle;
		
		// update current values
		position = botPosition;
		pieceIndex = position.getPiecePosition().getPieceIndex();
		laneDistanceOffset = track.getLanes().get(position.getPiecePosition().getLane().getEndLaneIndex()).getDistanceFromCenter();
		piece = track.getPieces().get(pieceIndex);
		angle = position.getAngle();
		distance = getDistanceTraveled();
		speed = distance/timeBetweenTicks;
		acceleration = speed - previousSpeed;
		lapSpeeds.add(speed);
		raceSpeeds.add(speed);
		if(previousPosition != null) enteredNewPiece = position.getPiecePosition().getPieceIndex() == previousPosition.getPiecePosition().getPieceIndex() + 1;
	}
	
	private double getDistanceTraveled() {
		double distance = position.getPiecePosition().getInPieceDistance();
		
		if(previousPiece != null) {
			if(position.getPiecePosition().getPieceIndex() == (previousPosition.getPiecePosition().getPieceIndex() + 1)) { // did it cross more than one piece in a single tick?
				double offset = 0;
				if(previousPiece.isTurn()) {
					offset = Math.abs(previousPiece.getArcLength() - laneDistanceOffset);
				} else {
					offset = previousPiece.getLength();
				}
				
				// TODO: take into account lane switching?
				
				distance = distance + offset;
			}
			
			distance = distance - previousPosition.getPiecePosition().getInPieceDistance();
		}
		
		return distance;
	}
	
	public void crashed() {
		crashes++;
		crashed = true;
	}
	
	public void spawned() {
		crashed = false;
	}
	
	public void lapFinished() {
		lapSpeeds.clear();
		lapTicks = 0;
	}
	
	public TurboAvailable getTurbo() {
		return turbo;
	}

	public void setTurbo(TurboAvailable turbo) {
		this.turbo = turbo;
	}
}
