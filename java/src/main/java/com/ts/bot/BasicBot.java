package com.ts.bot;

import java.util.LinkedList;
import java.util.logging.Logger;

import noobbot.domain.CarPositions;
import noobbot.domain.Msg;
import noobbot.domain.Piece;
import noobbot.domain.Race;
import noobbot.domain.TurboAvailable;

import com.ts.bot.connector.BotConnector;
import com.ts.bot.connector.SendMsg;
import com.ts.bot.connector.SendMsg.Join;
import com.ts.bot.connector.SendMsg.Throttle;
import com.ts.bot.connector.ServerMsg;
import com.ts.car.CarState;

/**
 * Can use this to easily fork and test new theories, as well as providing a base for simple analytics on performance over time
 * 
 * This is what they should've given us as our base model
 */
public class BasicBot {	
	private static final Logger log = Logger.getLogger(BasicBot.class.getName());
	
	protected final String name;
	protected final String key;
	
	protected Race race;
	protected LinkedList<Piece> trackPieces;
	protected CarState car;
	protected int totalLaps;

	public BasicBot(String name, String key) {
		this.name = name;
		this.key = key;
	}
	
	public String getName() {
		return name;
	}

	public String getKey() {
		return key;
	}
	
	/**
	 * Executes in response to successful join request when logging into server
	 */
	public void onJoin(ServerMsg fromServer) {
		log.info("Joined Successfully");
	}

	public void onJoinRace(ServerMsg fromServer) {
		log.info("Race joined successfully");
	}
	
	/**
	 * Comes back after joining the race, giving data about car joined
	 */
	public void onYourCar(ServerMsg fromServer) {
		log.info("Got car data");
	}
	
	/**
	 * Executes when race is initialized. Track data is given
	 */
	public void onGameInit(Msg initMsg) {
		log.info("Game initialized");
		race = initMsg.getData().getRace();
		trackPieces = race.getTrack().getPieces();
		car = new CarState(race.getTrack());
		try{
			totalLaps = initMsg.getData().getRace().getRaceSession().getLaps();
		}
		catch(Exception ex){
			log.info("if no lap then qualifying");
			totalLaps=0;
		}
	}
	
	/**
	 * Executes when race starts
	 */
	public void onRaceStart(ServerMsg fromServer) {
		log.info("Race Started");
	}

	/**
	 * Executes every time you have a decision to make. Respond with choice
	 */
	public void onCarPositions(CarPositions carPositionsMsg) {
		car.updateState(carPositionsMsg.getBotPosition(name));
	}
	
	public SendMsg getNextMove() {
		return new Throttle(0.5);
	}
	
	public void onTurboAvailable(TurboAvailable turbo){
		log.info("Turbo Available!!!");
		car.setTurbo(turbo);
	}

	
	/**
	 * Executes when crashed. 
	 * 
	 * Not sure if the msg is specific to this given bot. Needs more testing 
	 */
	public void onCrash(ServerMsg fromServer) {
		log.info("SKREEEEEEEEEEEEEEE CRASSSHHHHHHHHHHHHH PSSSHHHHHHHHHHHHH EXPLOSIONSSSS................");
		car.crashed();

		Piece piece = race.getTrack().getPieces().get(car.getPieceIndex());
		log.info(
				"Bot Angle: " + car.getAngle() + "\n" +
				"Piece Length: " + piece.getLength() + "\n" + 
				"Piece Angle: " + piece.getAngle() + "\n" + 
				"Piece Radius: " + piece.getRadius()
				);
	}
	
	/**
	 * Executes when respawning after crashing. 
	 * 
	 * Not sure if the msg is specific to this given bot. Needs more testing 
	 */
	public void onSpawn(ServerMsg fromServer) {
		log.info("Respawn");
	}

	/**
	 * Executes when crossing start point
	 * 
	 * Not sure if the msg is specific to this given bot. Needs more testing 
	 */
	public void onLapFinished(ServerMsg fromServer) {
		int lap =  car.getPosition().getPiecePosition().getLap();
		double avgSpeed = car.getAverageLapSpeed();
		log.info("Lap " + lap + " completed with an avg speed of " + avgSpeed);
		
		car.lapFinished();
	}
	
	/**
	 * Executes when crossing finish line.
	 * 
	 * Not sure if the msg is specific to this given bot. Needs more testing 
	 */
	public void onFinish(ServerMsg fromServer) {
		log.info("Finished");
	}
	
	/**
	 * Executes when Race ends
	 */
	public void onRaceEnd(ServerMsg fromServer) {
		log.info("Race Ended");
	}
	
	/**
	 * Should execute once everyone has finished and everyone is kicked out
	 */
	public void onTournamentEnd(ServerMsg fromServer) {
		log.info("Tournament ended.");
		printRaceAnalytics();
	}
	
	private void printRaceAnalytics() {
		System.out.println("\n\n\n\n\n"
				+ "Unofficial Race Analytics\n"
				+ "-------------------------\n"
				+ "Crashes: " + car.getCrashes() + "\n"
				+ "Avg Speed: " + car.getAverageRaceSpeed()
				);
	}
	
	public static void main(String... args) {
		String host = "testserver.helloworldopen.com";
		int port = Integer.parseInt("8091");
		String botName = "basic bot";
		String botKey = "6sJ4pZvYAUIApw";
		
		BasicBot bot = new BasicBot(botName, botKey);
		
		BotConnector serverConnection = new BotConnector(bot);
		serverConnection.race(host, port, new Join(bot.getName(), bot.getKey()));
	}
}
