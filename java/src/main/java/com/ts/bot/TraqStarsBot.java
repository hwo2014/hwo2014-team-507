package com.ts.bot;

import java.util.logging.Logger;

import noobbot.domain.CarPosition;
import noobbot.domain.Piece;
import noobbot.util.BotUtil;

import com.ts.bot.connector.BotConnector;
import com.ts.bot.connector.SendMsg;
import com.ts.bot.connector.SendMsg.Join;
import com.ts.bot.connector.SendMsg.JoinRace;
import com.ts.bot.connector.SendMsg.Switch;
import com.ts.bot.connector.SendMsg.Throttle;
import com.ts.bot.connector.SendMsg.Turbo;

public class TraqStarsBot extends BasicBot {
	private static final Logger log = Logger.getLogger(TraqStarsBot.class.getName());
	
	private double previousThrottle;

	public TraqStarsBot(String name, String key) {
		super(name, key);
	}

	int switchPiece = -1;
	private double turboTicks = 0;
	
	@Override
	public SendMsg getNextMove() {
		CarPosition ourBot = car.getPosition();
		
		int pieceIndex = ourBot.getPiecePosition().getPieceIndex();
		Piece nextNextPiece = trackPieces.get((pieceIndex + 2) % trackPieces.size());
		
		Piece x4turnPiece = trackPieces.get((pieceIndex + 4) % trackPieces.size());
		double x4turnAngle = x4turnPiece.getAngle();
		double nextNextTurnAngle = nextNextPiece.getAngle();
		double nextNextTurnAngleAbs = Math.abs(nextNextTurnAngle);
		double throttle = 0;
		
		if(!BotUtil.hasTurnsAhead(pieceIndex, trackPieces) && car.getTurbo() != null){ //car.getPosition().getPiecePosition().getLap() == (totalLaps ) && 
			log.info("TURBO!!!!!!");
			turboTicks = car.getTurbo().getData().getTurboDurationTicks();
			car.setTurbo(null);
			return new Turbo();
		}
		
		if(switchPiece != pieceIndex && (x4turnAngle > 0 && car.getPosition().getPiecePosition().getLane().getStartLaneIndex() == 0)){
			Piece afterSwitch = trackPieces.get((pieceIndex + 4) % trackPieces.size());
			log.info("lane after switch: " + afterSwitch.getAngle() );
			if(afterSwitch.getAngle() == 0 || (car.getSpeed() < 6 && car.getPreviousSpeed() < 6)){
				log.info("Sending Switch: " + Switch.SWITCH_RIGHT + " previous speed: " + car.getPreviousSpeed() + " speed: " + car.getSpeed());
				switchPiece = pieceIndex;
				return new Switch(Switch.SWITCH_RIGHT);
			}
		}
		else if(switchPiece != pieceIndex && (x4turnAngle < 0 && car.getPosition().getPiecePosition().getLane().getStartLaneIndex() == 1)){
			Piece afterSwitch = trackPieces.get((pieceIndex + 4) % trackPieces.size());
			if(afterSwitch.getAngle() == 0 || (car.getSpeed() < 6 && car.getPreviousSpeed() < 6)){
				log.info("Sending Switch: " + Switch.SWITCH_LEFT+ " previous speed: " + car.getPreviousSpeed() + " speed: " + car.getSpeed());
				switchPiece = pieceIndex;
				return new Switch(Switch.SWITCH_LEFT);
			}
		}
		
		if(nextNextTurnAngleAbs > 23) {
			throttle = .45;
		} else if(nextNextTurnAngleAbs > .25) {
			throttle = .85;
		} else {
			throttle = 1;
		}
//
//		if(car.getAngleChange() < -.5) {
//			throttle += .4;
//		} else 
			if(car.getAngleChange() <= 0) {
			throttle += .2;
		}else if(car.getAngleChange() > 2) {
			throttle = 0;
		} else if(car.getAngleChange() > 1) {
			throttle -= .2;
		}
		
		if(throttle > 1) {
			throttle = 1;
		} else if(throttle < 0) {
			throttle = 0;
		}
		
		if(throttle != this.previousThrottle) {
			log.info("Adjusted throttle: " + throttle );
			this.previousThrottle = throttle;
		}
		return new Throttle(throttle);
	}
	
	public static void execute(String...args){

//	public static void main(String... args) {
		 String host = args[0];
		 int port = Integer.parseInt(args[1]);
		 String botName = args[2];
		 String botKey = args[3]; 

//		String host = "testserver.helloworldopen.com";
//		int port = Integer.parseInt("8091");
//		String botName = "traqstars bot";
//		String botKey = "6sJ4pZvYAUIApw";

		TraqStarsBot bot = new TraqStarsBot(botName, botKey);
		
		BotConnector serverConnection = new BotConnector(bot);
//		String trackName = "keimola";
		String trackName = "usa";
//		String trackName = "germany";
		String password = null;
		int numberOfBots = 1;
		serverConnection.race(host, port, new Join(bot.getName(), bot.getKey()));
//		serverConnection.race(host, port, new JoinRace(bot.getName(), bot.getKey(), trackName, password, numberOfBots));
	}
}
