package com.ts.bot.connector;


/**
 * These are messages that can be sent back to the server
 * 
 * @author Brad.Bowie
 *
 */
public abstract class SendMsg {
	public ServerMsg toServerMsg() {
		return new ServerMsg(this.msgType(), this.msgData(), null);
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
	
	///////////////////////////////////////////////////////////////////////////////////////////
	// Msg classes listed internally below. They will be going to server on behalf of our bot
	// Their main job is to wrap the data blobs found within ServerMsg and assign the msg type
	///////////////////////////////////////////////////////////////////////////////////////////
	
	public static class Join extends SendMsg {
		public final String name;
		public final String key;

		public Join(final String name, final String key) {
			this.name = name;
			this.key = key;
		}

		@Override
		protected String msgType() {
			return "join";
		}
	}
	
	public static class JoinRace extends SendMsg {
		public final BotId botId;
		public final String trackName;
		public final String password;
		public final int carCount;

		public JoinRace(String name, String key, String trackName, String password, int carCount) {
			botId = new BotId(name, key);
			this.trackName = trackName;
			this.password = password;
			this.carCount = carCount;
		}

		@Override
		protected String msgType() {
			return "joinRace";
		}
		
		class BotId {
			public final String name;
			public final String key;
			
			public BotId(String name, String key) {
				this.name = name;
				this.key = key;
			}
		}
	}

	public static class Throttle extends SendMsg {
		private double value;

		public Throttle(double value) {
			this.value = value;
		}

		@Override
		protected Object msgData() {
			return value;
		}

		@Override
		protected String msgType() {
			return "throttle";
		}
	}

	public static class Switch extends SendMsg {
		// TODO: verify these constants are correct
		public static final String SWITCH_RIGHT = "Right";
		public static final String SWITCH_LEFT = "Left";
		
		private String value; // left or right

		public Switch(String value) {
			this.value = value;
		}

		@Override
		protected Object msgData() {
			return value;
		}

		@Override
		protected String msgType() {
			return "switchLane";
		}
	}

	public static class Ping extends SendMsg {
		@Override
		protected String msgType() {
			return "ping";
		}
	}
	
	public static class Turbo extends SendMsg {
		private String value = "Go Go gadget wheels.";

		public Turbo() {
		}

		@Override
		protected Object msgData() {
			return value;
		}

		@Override
		protected String msgType() {
			return "turbo";
		}
	}
}


