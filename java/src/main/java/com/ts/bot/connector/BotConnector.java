package com.ts.bot.connector;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import noobbot.domain.CarPositions;
import noobbot.domain.Msg;
import noobbot.domain.TurboAvailable;

import com.google.gson.Gson;
import com.ts.bot.BasicBot;
import com.ts.bot.connector.SendMsg.Ping;

/**
 * Connects bot to server and maps all msgs into the bot logic. 
 * 
 * Also wraps the fugly connection logic so we don't have to look at it again... ever
 * @author Brad.Bowie
 *
 */

public class BotConnector {
	private static final Logger log = Logger.getLogger(BotConnector.class.getName());
	
    private final ConsoleHandler handler;
	private final Gson gson;
    private final String raceFileDirectory = "C:/hwo/raceFiles/";
	
	private Socket socket;
	private BufferedReader reader;
	private PrintWriter writer;
//	private BufferedWriter raceFileWriter;
	private BasicBot bot;
	
	public BotConnector(BasicBot bot) {
		this.bot = bot;
		
		gson = new Gson();
		
//		File fileDir = new File(raceFileDirectory);
//		if(!fileDir.exists()) fileDir.mkdirs();
		
		handler = new ConsoleHandler();
		handler.setLevel(Level.INFO);
		log.addHandler(handler);
	}
	
	public void setVerbose(boolean verbose) {
		if(verbose) {
			handler.setLevel(Level.FINEST);
			log.setLevel(Level.FINEST);
		} else {
			handler.setLevel(Level.INFO);
			log.setLevel(Level.INFO);
		}
		
		log.info("Verbose output: " + verbose);
	}

	public void race(String host, int port, SendMsg joinMsg) {
		try {
			openConnection(host, port);
			send(joinMsg);
			handleRaceInteractions();
		} finally {
			destroyConnection();
		}
	}
	
	private void openConnection(String host, int port) {
		log.info("Connecting to " + host + ":" + port);
        try {
            socket = new Socket(host, port);
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
            
            
            log.finest("Connection successful");
        } catch(IOException e) {
        	e.printStackTrace();
        }
	}

	private void initRaceFile(String raceId) {
//        try {
//        	String fileName =  raceId + ".race";
//        	raceFileWriter = new BufferedWriter(new FileWriter(new File(raceFileDirectory + fileName)));
//        } catch(IOException e) {
//        	e.printStackTrace();
//        }
        
	}
	
	/**
	 * Will run until server kills the connection... for whatever reason
	 */
	private void handleRaceInteractions() {
		log.finest("Receiving data from server...");
		String line;
		try {
	        while((line = reader.readLine()) != null) {
//	        	if(raceFileWriter != null) raceFileWriter.write(line + "\n");
	        	
	        	log.finest("Got from server: " + line);
	        	ServerMsg fromServer = gson.fromJson(line, ServerMsg.class);
	        	SendMsg response = null;
	    		
	        	// TODO: can expand on these later if we want to add more functionality
	    		switch(fromServer.msgType) {
	    		case "join":
	    			bot.onJoin(fromServer);
	    			break;
	    		case "joinRace":
	    			bot.onJoinRace(fromServer);
	    			break;
	    		case "yourCar": 
	    			bot.onYourCar(fromServer);
	    			break;
	    		case "gameInit":
	    			initRaceFile(fromServer.gameId);
	    			bot.onGameInit(gson.fromJson(line, Msg.class));
	    			break;
	    		case "gameStart":
	    			bot.onRaceStart(fromServer);
	    			break;
	    		case "gameEnd":
	    			bot.onRaceEnd(fromServer);
	    			break;
	    		case "carPositions":
	    			bot.onCarPositions(gson.fromJson(line, CarPositions.class));
	    			response = bot.getNextMove();
	    			break;
	    		case "lapFinished":
	    			bot.onLapFinished(fromServer);
	    			break;
	    		case "crash":
	    			bot.onCrash(fromServer);
	    			break;
	    		case "spawn":
	    			bot.onSpawn(fromServer);
	    			break;
	    		case "finish":
	    			bot.onFinish(fromServer);
	    			break;
	    		case "tournamentEnd":
	    			bot.onTournamentEnd(fromServer);
	    			break;
	    		case "error":
	    			log.warning("Error received: " + fromServer.data);
	    			break;
	    		case "turboAvailable":
	    			bot.onTurboAvailable( gson.fromJson(line, TurboAvailable.class));
	    			break;
	    		default:
	    			log.info("Unexpected msg type: " + fromServer.msgType + " \n"
	    					+ fromServer.data + "\n"
	    	    			+ "\nResponding with ping");
	    	    	response = new Ping();
	    			break;
	    		}

        		send(response);
	        }
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		log.info("Server closed connection");
	}
	
	private void send(SendMsg msg) {
		if(msg == null) return;
		
    	String output = gson.toJson(msg.toServerMsg());
    	log.finest("Sending " + output);
        writer.println(output);
        writer.flush();
	}

	private void destroyConnection() {
		log.finest("Cleaning up connection data");
		if(socket != null) {
			try {
				if(reader != null) reader.close();
				if(writer != null) writer.close();
//				if(raceFileWriter != null) raceFileWriter.close();
				if(socket != null) socket.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}
