package com.ts.bot.connector;

/**
 * This is a wrapper of all messages to and from the server
 * 
 * @author Brad.Bowie
 *
 */
public class ServerMsg {
	public final String msgType;
	public final Object data;
	public final String gameId;

	ServerMsg(final String msgType, final Object data, String gameId) {
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
	}
}